# NemFi
_NemFi_ a trace-driven WiFi emulator. It is a record-and-replay tool that can capture traces representing real WiFi conditions, and later replay these traces to reproduce the same conditions. State-of-the-art cellular emulator cannot emulate WiFi conditions. We need to address some key features to enable accurate WiFi record-and-replay; namely: WiFi packet losses, medium-access control, and frame-aggregation. _NemFi_ extends and re-designs the existing cellular network emulator to incorporate these properties to support WiFi record-and-replay. We evaluate the performance of _NemFi_ against repeated experimentation across different WiFi conditions and for three different types of applications: speed-test, file download, and video streaming. Our experimental results demonstrate that _NemFi_ achieves accurate record-and-replay as we obtain 97\% similar application performance using _NemFi_ versus repeated experimentation in similar settings.

The work is done at Inria, Paris and is published in ACM SIGCOMM, Computer Communication Review (CCR) 2021. Abhishek kumar Mishra, Sara Ayoubi, Giulio Grassi, and Renata Teixeira. 2021. NemFi: record-and-replay to emulate WiFi. <i>SIGCOMM Comput. Commun. Rev.</i> 51, 3 (July 2021), 2–8. DOI:https://doi.org/10.1145/3477482.3477484 

Principal invertigators:
- Abhishek Mishra (Inria, Paris)
- Giulio Grassi (Cisco, Paris)
- Sara Ayoubi (Nokia Bell Labs, Paris)
- Renata Teixeira (Inria, Paris)

## Installation
Install the dependency:

```bash
tar xvzf netifaces-0.10.8.tar.gz
cd netifaces-0.10.8
python setup.py install
```

## File Overview - Key Modules
### NemFi's Record
1. [Client Rate Control](client_saturator/multisend/sender/saturateservo.cc) - Rate control by the client.
2. [Client Saturator](client_saturator/multisend/sender/saturatr.cc) - Client link saturation application.
3. [Server Rate Control](server_saturator/multisend/sender/saturateservo.cc) - Rate control by the server.
4. [Server Saturator](server_saturator/multisend/sender/saturatr.cc) - Server link saturation application.
5. [Client State Machine Record](client_saturator/multisend/sender/statem-saturatr.cc) - Maintains client state.
6. [Rate estimate at Server](server_saturator/multisend/sender/rate-estimate.cc) - Rate estimation of the saturation link at the server.

### NemFi's Replay
1. [Delivery opportunities, Frame aggregation and WiFi only losses](mahimahi/src/frontend/link_queue.cc)
2. [Uplink and downlink queue sharing](mahimahi/src/packet/packetshell.cc)
3. [Replaying the WiFi link](mahimahi/src/frontend/mm-link)

### Utilities
1. [Run NemFi's Record and Replay instances](nemfi.sh)
2. [Application/protocol evaluation script](protocol_application__evaluation.sh)
3. [Set IP routing tables](iproutes_working.py)
4. [Set local network buffer size](set_network_buffer.sh)
5. [Set client saturator parameters](client_saturator/multisend/sender/data_collection_client.py)
6. [Client trace collection](client_saturator/multisend/sender/nemfi_client.sh)
7. [Set server saturator parameters](server_saturator/multisend/sender/data_collection_server.py)
8. [Server trace collection](server_saturator/multisend/sender/nemfi_server.sh)
9. [NemFi replay script](mahimahi/nemfi_replay.sh)
10. [Covert recorded traces to replay trace format](mahimahi/src/wifi-util/convert_trace.py)

## Usage

- For the experiemntal setup, follow the paper referenced above.
- Run the following base script on both the client and server machines(Linux).
```bash
./nemfi.sh
```
- The script is interative. Specify record and replay parameters.
- If setup and run correctly, collected client and server traces could be found at:
server_saturator/multisend/sender/Client_Logs/ and server_saturator/multisend/sender/Server_Logs/ respectively.
- Collected traces for the scenarios in the reference paper are present in the folder traces/

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Contact
If facing any issues in building or using NemFi, please feel free to contact: Abhishek MISHRA, email: abhishek.mishra@inria.fr

## License
[MIT](https://choosealicense.com/licenses/mit/)
