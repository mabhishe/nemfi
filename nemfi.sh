#!/bin/bash

#Ask for pre-configuration (Running for the first time in the test session)
read -p "Running NemFi for the first time on the machine(true/false): " isconfig

if $isconfig #do the pre-configuration
then
	echo "--- Setting IP route tables ---"
	read -p "Enter data interface: " diface
	read -p "Enter feedback interface: " fiface

	#sudo python3 iproutes_working.py --i1 diface --i2 fiface
	echo "--- IP route tables set ---"

	echo "--- Setting local network buffer size ---"
	#chmod +x set_network_buffer.sh
	#sudo ./set_network_buffer.sh
	echo "--- Local network buffer size set ---"

	echo "Pre-congiguration done!"
fi

#Ask if it is client ot not(server)

read -p "Is this machine Client(true)/Server(false): " isclient
read -p "Number of record runs: " num_runs
read -p "Replay(true/false): " isreplay #To be optionally True only on the client machine!

#NEMFI's RECORD SPECIFIC VARIABLES
record_duration=100 #Record duration in seconds
client_ip="192.168.0.186" #Client machine's IP address
server_ip="192.168.0.123" #server machine's IP address
inter_record_gap=3 #interval between two record sessions of NemFi

saturation_direction="UP" #Uplink(UP) mode by default. Could also be Downlink(DOWN) or, Bi-directional(BI). Don't change this unless experimenting!!

if $isclient #run the client instance
then
	cd client_saturator/multisend/sender/
	for i in $(seq 1 $num_runs)
	do
		echo "NemFi's Client Record: iteration "$i
		./nemfi_client.sh diface fiface record_duration saturation_direction server_ip
		sleep $inter_record_gap
	done
	cd ../../../

else #run the server instance
	cd server_saturator/multisend/sender/
	for i in $(seq 1 $num_runs)
	do
		echo "NemFi's Server Record: iteration "$i
		./nemfi_server.sh record_duration client_ip saturation_direction
		sleep $((inter_record_gap - 1)) #Server needs to start a bit early to listen. Might cause small offset for high record runs
	done
	cd ../../../
fi





#Enter your desired application here!!!! It's the same one you would like to emulate in the Replay phase next
#Run obviously on both the client and the server with required instances

#./protocol_application__evaluation.sh



#NEMFI's REPLAY SPECIFIC VARIABLES

if $isreplay #Executing NemFi's Replay instance. To be executed only on the client machine!
then
	cd mahimahi/
	make
	sudo make install
	echo "NemFi's Client Replay: iteration "$i
	./nemfi_replay.sh num_runs
	cd ../
fi

exit
