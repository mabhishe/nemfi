#include <string>
#include <vector>
#include <poll.h>
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>
#include <memory>
#include <stdexcept>
#include <array>

#include "acker.hh"
#include "saturateservo.hh"
#include "statem-saturatr.hh"

std::string exec(const char *cmd)
{
  std::array<char, 128> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
  if (!pipe)
  {
    throw std::runtime_error("popen() failed!");
  }
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
  {
    result+=buffer.data();
  }
  return result;
}

using namespace std;

int main( int argc, char *argv[] )
{
  if ( argc != 1 && argc != 7 ) {
    fprintf( stderr, "Usage: %s [RELIABLE_IP RELIABLE_DEV TEST_IP TEST_DEV SERVER_IP CLIENT_MODE]\n",
	     argv[ 0 ]);
    exit( 1 );
  }

  Socket data_socket, feedback_socket;
  bool server;

  int sender_id = getpid();

  const char *client_mode=NULL;

  Socket::Address remote_data_address( UNKNOWN ), remote_feedback_address( UNKNOWN );

  uint64_t ts=Socket::timestamp();
  if ( argc == 1 ) { /* server */
    server = true;
    data_socket.bind( Socket::Address( "0.0.0.0", 9001 ) );
    feedback_socket.bind( Socket::Address( "0.0.0.0", 9002 ) );
  } else { /* client */
    server = false;
    
    const char *reliable_ip = argv[ 1 ];
    const char *reliable_dev = argv[ 2 ];

    const char *test_ip = argv[ 3 ];
    const char *test_dev = argv[ 4 ];

    const char *server_ip = argv[ 5 ];

    client_mode = argv[ 6 ]; 

    sender_id = ( (int)(ts/1e9) );

    data_socket.bind( Socket::Address( test_ip, 9003 ) );
    data_socket.bind_to_device( test_dev );
    remote_data_address = Socket::Address( server_ip, 9001 );

    feedback_socket.bind( Socket::Address( reliable_ip, 9004 ) );
    feedback_socket.bind_to_device( reliable_dev );
    remote_feedback_address = Socket::Address( server_ip, 9002 );
  }

  FILE* log_file;
  char log_file_name[50];
  sprintf(log_file_name,"%s-%d-%d",server ? "server" : "client",(int)(ts/1e9),sender_id);
  log_file=fopen(log_file_name,"w");

  //int given_Lower_Window = 20;
  //int given_Upper_Window = 800;

  //double given_Lower_RTT = 0.75;
  //double given_Upper_RTT = 3.0;

  //printf("***************************************Initializing*********************************************\n");

  std::string stndrd_wifi = "802.11";

  int mcs_uplink = std::stoi(exec((string("iw dev ") + string(argv[ 4 ]) + string(" station dump | awk '$1 == \"tx\" && $2 == \"bitrate:\" {print $6}'")).c_str()));
  int mcs_downlink = std::stoi(exec((string("iw dev ") + string(argv[ 4 ]) + string(" station dump | awk '$1 == \"rx\" && $2 == \"bitrate:\" {print $6}'")).c_str()));
  double bitrate_uplink = std::stod(exec((string("iw dev ") + string(argv[ 4 ]) + string(" station dump | awk '$1 == \"tx\" && $2 == \"bitrate:\" {print $3}'")).c_str()));
  double bitrate_downlink = std::stod(exec((string("iw dev ") + string(argv[ 4 ]) + string(" station dump | awk '$1 == \"rx\" && $2 == \"bitrate:\" {print $3}'")).c_str()));
  //int current_bandwidth = std::stoi(exec((string("iw dev ") + string(argv[ 4 ]) + string(" station dump | awk '$1 == \"tx\" && $2 == \"bitrate:\" {print $7}'")).c_str()));
  int current_bandwidth = 20;
  double iface_card_sensitivity = -87.5;
  
  static statem_saturatr statemsat(mcs_uplink, mcs_downlink, bitrate_uplink, bitrate_downlink, stndrd_wifi, string(client_mode), current_bandwidth, iface_card_sensitivity, string(argv[ 4 ]));

  SaturateServo saturatr( "OUTGOING", log_file, feedback_socket, data_socket, remote_data_address, server, sender_id );
  Acker acker( "INCOMING", log_file, data_socket, feedback_socket, remote_feedback_address, server, sender_id );

  saturatr.set_acker( &acker );
  saturatr.set_statem( &statemsat );
  saturatr.start_update(25);

  acker.set_saturatr( &saturatr );
  acker.set_statem( &statemsat );

  //printf("%d %d %f %f %d\n", mcs_uplink, mcs_downlink, bitrate_uplink, bitrate_downlink, current_bandwidth);

  //printf("**********************************Initialization Finished***************************************\n");

  int count_start=0;

  while ( true ) {
    fflush( NULL );

    /* possibly send packet according to mode asked*/

    if (!strcmp(client_mode, "UP")){
	saturatr.tick();
	acker.tick();
}

   else if (!strcmp(client_mode, "DOWN")){
 	if (count_start<5){
    		saturatr.tick();
    		count_start++;
   	 	}
    	acker.tick();
	}

    else if (!strcmp(client_mode, "BI")){
	saturatr.tick();
	acker.tick();
}
    
    /* wait for incoming packet OR expiry of timer */
    struct pollfd poll_fds[ 2 ];
    poll_fds[ 0 ].fd = data_socket.get_sock();
    poll_fds[ 0 ].events = POLLIN;
    poll_fds[ 1 ].fd = feedback_socket.get_sock();
    poll_fds[ 1 ].events = POLLIN;

    struct timespec timeout;
    uint64_t next_transmission_delay = std::min( saturatr.wait_time(), acker.wait_time() );

    /*if ( next_transmission_delay == 0 ) {
      fprintf( stderr, "ZERO %ld %ld\n", saturatr.wait_time(), acker.wait_time() );
    }*/

    timeout.tv_sec = next_transmission_delay / 1000000000;
    timeout.tv_nsec = next_transmission_delay % 1000000000;
    ppoll( poll_fds, 2, &timeout, NULL );

    if ( poll_fds[ 0 ].revents & POLLIN ) {
      acker.recv();
    }

    if ( poll_fds[ 1 ].revents & POLLIN ) {
      saturatr.recv();
    }
  }
}
