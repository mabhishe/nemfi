#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess, time, signal
import argparse, os, shutil
from os import path
import netifaces as ni
import socket

import re
#import matplotlib.pyplot as plt
#import numpy as np
from os import listdir
from os.path import isfile, join

directory = "Client_Logs"
if not os.path.exists(directory):
	os.makedirs(directory)

timeStr = int(time.time())
folder = ""

os.chmod(directory, 0o777)

def set_sat_params(UPPER_WINDOW ,LOWER_WINDOW=5, LOWER_RTT=1.5, UPPER_RTT=3.0):
	print('Setting Saturator parameter, Upper Window: ', UPPER_WINDOW)
	file = open("specify_params.hh", "w")
	file.write("const int given_Lower_Window={};".format(LOWER_WINDOW) + '\n')
	file.write("const int given_Upper_Window={};".format(UPPER_WINDOW) + '\n')
	file.write("constexpr double given_Lower_RTT={};".format(LOWER_RTT) + '\n')
	file.write("constexpr double given_Upper_RTT={};".format(UPPER_RTT) + '\n')
	file.close()

def saturator(destination, wifi_ip, feedback_ip, wifi_interface, feedback_interface, duration, test_done, do_analysis, client_mode, wind_val):

	global folder
	
	get_current_directory = subprocess.Popen(['pwd'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
	cwd_out, cwd_err = get_current_directory.communicate()
	cwd_out = cwd_out.decode('utf-8')[:-1]	
	
	#clean_make = subprocess.Popen(['make', '-C', '/media/ubuntu/79bf333c-7b40-4765-9e0a-c16fa2b41163/sprout/multisend/sender/', 'clean'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
	#clean_make.wait()
	update_make = subprocess.Popen(['make', 'saturatr'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True, cwd=cwd_out)
	#print("Make Updated")
	stdout, stderr = update_make.communicate()
	#print(stdout, stderr)

	start_time = time.time()

	sat_wifi = subprocess.Popen(['nice', '-19', cwd_out + '/saturatr', feedback_ip, feedback_interface, wifi_ip, wifi_interface, destination, client_mode], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True, preexec_fn=os.setpgrp)
	print('saturator wifi client')

	while (time.time() - start_time < duration-1):
		pass
	#sat_wifi.terminate()
	sat_wifi.send_signal(signal.SIGINT)

	stdout, stderr = sat_wifi.communicate()
	if stderr:
		print("Saturator WiFi ERROR ----------", stderr)

	folder = "Client_Logs/Data_"+str(timeStr)+"_"+client_mode+"_"+ str(wind_val)+ "/"
	if not os.path.exists(folder):
		os.makedirs(folder)

	os.chmod(folder, 0o777)	

	source = os.getcwd()
	files = [i for i in os.listdir(source) if i.startswith("client-") and path.isfile(path.join(source, i))]
	for f in files:
		shutil.move(path.join(source, f), folder)

	return test_done


def uplink_iperf(destination, server_port):

	global folder
	timeStr = int(time.time())
	sender_id = int(os.getpid())

	filename_wifi = "{0}_{1}_{2}".format("client_iperf_wifi",timeStr,sender_id)
	file_wifi = open(filename_wifi, "w")

	iperf_wifi = subprocess.Popen(['iperf', '-c', destination, '-i', '10',  '-p', server_port, '-t', 30], stdout=file_wifi)
	print('iperf wifi')

	iperf_wifi.wait()
	iperf_wifi.send_signal(signal.SIGINT)
	file_wifi.close()

	source = os.getcwd()
	files = [i for i in os.listdir(source) if i.startswith("client_") and path.isfile(path.join(source, i))]
	for f in files:
		shutil.move(path.join(source, f), folder)


def main():

	parser = argparse.ArgumentParser(description = "Client Trace Collection")
	parser.add_argument('wifi', type = str, help='WiFi Interface Name')
	parser.add_argument('feedback', type = str, help='Feedback Interface Name')
	parser.add_argument('T', type = int, help='Time_Duration in Seconds')
	parser.add_argument('mode', type=str, help='Mode of Transmissions: UP, DOWN or BI')
	parser.add_argument('--location', type = str, help='Location of test', required = False, default = 'Inria de Paris')
	parser.add_argument('dest', type = str, help='Destination address of the server')
	parser.add_argument('test_lwin', type = int, help='Test Lower Window')
	parser.add_argument('test_uwin', type = int, help='Test Upper Window')
	parser.add_argument('--mobility', type = str, help='stationary or mobile while conducting the test', required = False, default = 'stationary')
	parser.add_argument('--saturator', type = bool, help='Run Saturator Experiment - BOOLEAN', required = False, default = True)
	parser.add_argument('--anlyz', type = bool, help='Perform Analysis over data - BOOLEAN', required = False, default = False)
	parser.add_argument('--iprf', type = bool, help='Run Iperf Experiment - BOOLEAN', required = False, default = False)
	parser.add_argument('--iperf_port', type = int, help='Iperf Sever Port', required = False, default = 5001)

	args = parser.parse_args()

	wifi_ip = ni.ifaddresses(args.wifi)[ni.AF_INET][0]['addr']
	wifi_interface = args.wifi
	feedback_ip = ni.ifaddresses(args.feedback)[ni.AF_INET][0]['addr']
	feedback_interface = args.feedback
	destination = args.dest
	server_port = args.iperf_port
	client_mode = args.mode
	do_analysis = args.anlyz

	duration = args.T

	sat = args.saturator

	sender_id = str(os.getpid())
	location = args.location	
	mobility = args.mobility
	#ssid = subprocess.check_output(['iwgetid', '-r'])
	ssid = "test ssid"
	#ssid = str(ssid)
	ssid = ssid[2:len(ssid)-3]
	global timeStr

	source = os.getcwd()
	files = [i for i in os.listdir(source) if i.startswith("client-") or i.startswith("metadata_")]
	for f in files:
		if os.path.isfile(f):
			os.remove(f)

	test_done = False

	window_list=[]
	step=1000
	for val in range(args.test_lwin, args.test_uwin+1, step):
		window_list.append(val)

	if (args.saturator == True and args.iprf == True):
		for val in window_list:
			set_sat_params(val)
			test_done = saturator(destination, wifi_ip, feedback_ip, wifi_interface, feedback_interface, duration, test_done, do_analysis, client_mode, val)
			time.sleep(2)
		if test_done:
			print("Saturator Client Test Completed")
		time.sleep(30)
		uplink_iperf(destination, server_port)
		print("Iperf Client Test Completed")

	#if args.iprf or args.ping and not args.saturator and destination != '128.93.101.20':
		#subprocess.Popen(['ssh', '-i', 'Documents/msyedparis.pem', 'ubuntu@ec2-52-47-90-214.eu-west-3.compute.amazonaws.com', 'mkdir', folder])

	elif (args.saturator == True and args.iprf == False):
		for val in window_list:
			set_sat_params(val)
			test_done = saturator(destination, wifi_ip, feedback_ip, wifi_interface, feedback_interface, duration, test_done, do_analysis, client_mode, val)
			time.sleep(2)
		if test_done:
			print("Saturator Client Test Completed")

	elif args.iprf:
		uplink_iperf(destination, server_port)
		print("Iperf Client Test Completed")
		test_done = True

	metadata = "{0}_{1}_{2}".format("metadata",timeStr,sender_id)
	file = open(metadata, "w")
	file.write('Sender_ID:' + sender_id + '\n')
	file.write('WiFi_AP:' + ssid + '\n')
	file.write('Location:' + location + '\n')
	file.write('Time:' + str(timeStr) + '\n')
	file.write('Test_Duration:' + str(duration) + '\n')
	file.write('Destination:' + str(destination) + '\n')
	file.write('Mobility:' + mobility)
	file.write('\n')
	file.close()
	
	files = [i for i in os.listdir(source) if i.startswith("metadata") and path.isfile(path.join(source, i))]
	for f in files:
		shutil.move(path.join(source, f), folder)

if __name__ == '__main__':
	main()
