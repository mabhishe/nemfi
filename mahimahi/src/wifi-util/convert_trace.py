import glob
import sys

all_traces = glob.glob("../../traces/client*")

class convert_trace():
	def __init__(self):
		self.writer = "../../traces/test_file.txt"
	def convert(self):
		flag=0
		try:
			self.writer=open(self.writer, 'w')
		except IOError:
			print("IOError")
		for line in open(all_traces[0]).readlines():
			#print(line)
			split_line = line.split()
			send_time = split_line[0].split('=')[1]
			if flag==0:
				send_time= int(send_time)
				init_time = send_time
				flag=1

			else:
				send_time = (int(send_time) - init_time)/1000000
			#print(send_time, float(split_line[4].split('=')[1]), int(split_line[2].split('=')[1]), float(split_line[5].split('=')[1]))
			#if (float(split_line[4].split('=')[1])!=0):
			self.writer.write("%f %f %d %f\n" %(send_time, float(split_line[4].split('=')[1]), int(split_line[2].split('=')[1]), float(split_line[5].split('=')[1])))
		self.writer.close()

def main():
	con_trace = convert_trace()
	con_trace.convert()

if __name__ == '__main__':
	main()
