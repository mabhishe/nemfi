/* -*-mode:c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#ifndef LOSS_QUEUE_HH
#define LOSS_QUEUE_HH

#include <queue>
#include <cstdint>
#include <string>
#include <random>

#include "file_descriptor.hh"

class LossQueue
{
private:
    std::queue<std::string> packet_queue_ {};

    float percent_sharing_;

    virtual bool drop_packet( const std::string & packet ) = 0;

protected:
    std::default_random_engine prng_;

public:
    LossQueue();
    virtual ~LossQueue() {}

    void read_packet( const std::string & contents, bool direction);

    void write_packets( FileDescriptor & fd, bool direction);

    unsigned int wait_time( bool direction );

    bool pending_output( bool direction __attribute__((unused)) ) const { return not packet_queue_.empty(); }

    static bool finished( void ) { return false; }

    float percent_sharing() const { return percent_sharing_; }
};

class IIDLoss : public LossQueue
{
private:
    std::bernoulli_distribution drop_dist_;

    bool drop_packet( const std::string & packet ) override;

public:
    IIDLoss( const double loss_rate ) : drop_dist_( loss_rate ) {}
};

class SwitchingLink : public LossQueue
{
private:
    bool link_is_on_;
    std::exponential_distribution<> on_process_;
    std::exponential_distribution<> off_process_;

    uint64_t next_switch_time_;

    void calculate_next_switch_time( void );

    bool drop_packet( const std::string & packet ) override;

public:
    SwitchingLink( const double mean_on_time_, const double mean_off_time );

    unsigned int wait_time( bool direction );
};

#endif /* LOSS_QUEUE_HH */
