/* -*-mode:c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#ifndef DELAY_QUEUE_HH
#define DELAY_QUEUE_HH

#include <queue>
#include <cstdint>
#include <string>

#include "file_descriptor.hh"

class DelayQueue
{
private:
    uint64_t delay_ms_;
    std::queue< std::pair<uint64_t, std::string> > packet_queue_;

    float percent_sharing_;
    /* release timestamp, contents */

public:
    DelayQueue( const uint64_t & s_delay_ms ) : delay_ms_( s_delay_ms ), packet_queue_(), percent_sharing_(0.5) {}

    void read_packet( const std::string & contents, bool direction);

    void write_packets( FileDescriptor & fd, bool direction);

    unsigned int wait_time( bool direction ) const;

    bool pending_output( bool direction __attribute__((unused)) ) const { return wait_time(true) <= 0; }

    static bool finished( void ) { return false; }

    float percent_sharing() const { return percent_sharing_; }
};

#endif /* DELAY_QUEUE_HH */
