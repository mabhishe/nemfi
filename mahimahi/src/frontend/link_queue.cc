/* -*-mode:c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include <limits>
#include <cassert>

#include <random>
#include <iostream>

#include "link_queue.hh"
#include "timestamp.hh"
#include "util.hh"
#include "ezio.hh"
#include "abstract_packet_queue.hh"

using namespace std;

LinkQueue::LinkQueue( const std::string & link_name, const std::string & filename __attribute__((unused)) , const std::string & logfile __attribute__((unused)) , const float percent_sharing,
               const bool repeat, const bool graph_throughput __attribute__((unused)), const bool graph_delay __attribute__((unused)),
               std::unique_ptr<AbstractPacketQueue> && packet_queue, const std::string & command_line __attribute__((unused)) , const bool qd __attribute__((unused)) , const uint64_t rtt_server,
               PacketShell<LinkQueue> &link_shell_app, unsigned int &num_pack_up, unsigned int &num_pack_down, unsigned int &global_next_delivery )
    : link_name_(link_name),
      next_delivery_( 0 ),
      percent_sharing_( percent_sharing ),
      schedule_(),
      rate_schedule_(),
      sequence_numbers_(),
      losses_(),
      base_timestamp_( timestamp() ),
      delay_ms_( rtt_server ),
      packet_queue_( move( packet_queue ) ),
      packet_in_transit_("", 0),
      packet_in_transit_bytes_left_( 0 ),
      output_queue_(),
      log_(),
      throughput_graph_( nullptr ),
      delay_graph_( nullptr ),
      repeat_( repeat ),
      finished_( false ),
      first_loop_( true ),
      qdirection_ ( qd ),
      num_pack_up_(num_pack_up),
      num_pack_down_(num_pack_down),
      global_next_delivery_(global_next_delivery),
      link_shell_app_(link_shell_app),
      saturation_index_(0)
{
    //assert_not_root();

    /* open filename and load schedule */
    ifstream trace_file( filename );

    if ( not trace_file.good() ) {
        throw runtime_error( filename + ": error opening for reading" );
    }

    string line;

    srand (static_cast <unsigned> (time(0)));

    while ( trace_file.good() and getline( trace_file, line ) ) {
        if ( line.empty() ) {
            throw runtime_error( filename + ": invalid empty line" );
        }

        std::string delimiter = " ";
        uint64_t ms, seq;
        float msf, rate, loss;

        sscanf(line.c_str(), "%f %f %lu %f", &msf, &rate, &seq, &loss);

        ms = static_cast<uint64_t>(msf);

	if ( ms < 5000000)//ms > 10 &&
	{
		schedule_.emplace_back( ms );
    	rate_schedule_.emplace_back( 144.40 );//rate
    	sequence_numbers_.emplace_back( seq );
    	losses_.emplace_back(loss/100);
	}
        if (first_loop_ && ms < 5000000)//2500 && ms > 10
        {
            first_loop_=false;
            saturation_index_=schedule_.size();
        }
    }

    if ( schedule_.empty() ) {
        throw runtime_error( filename + ": no valid timestamps found" );
    }

    if ( schedule_.back() == 0 ) {
        throw runtime_error( filename + ": trace must last for a nonzero amount of time" );
    }

    /* open logfile if called for */
    if ( not logfile.empty() ) {
        log_.reset( new ofstream( logfile ) );
        if ( not log_->good() ) {
            throw runtime_error( logfile + ": error opening for writing" );
        }

        *log_ << "# mahimahi mm-link (" << link_name << ") [" << filename << "] > " << logfile << endl;
        *log_ << "# command line: " << command_line << endl;
        *log_ << "# queue: " << packet_queue_->to_string() << endl;
        *log_ << "# init timestamp: " << initial_timestamp() << endl;
        *log_ << "# base timestamp: " << base_timestamp_ << endl;
        *log_ << "# Trace Vecor Sizes: Shedule Rate Losses: " << schedule_.size() << rate_schedule_.size() << losses_.size() << endl;
        const char * prefix = getenv( "MAHIMAHI_SHELL_PREFIX" );
        if ( prefix ) {
            *log_ << "# mahimahi config: " << prefix << endl;
        }
    }

    /* create uplink graphs if called for */
    if ( graph_throughput ) {
        throughput_graph_.reset( new BinnedLiveGraph( link_name + " [" + filename + "]",
                                                      { make_tuple( 1.0, 0.0, 0.0, 0.25, true ),
                                                        make_tuple( 0.0, 0.0, 0.4, 1.0, false ),
                                                        make_tuple( 1.0, 0.0, 0.0, 0.5, false ) },
                                                      "throughput (Mbps)",
                                                      8.0 / 1000000.0,
                                                      true,
                                                      500,
                                                      [] ( int, int & x ) { x = 0; } ) );
    }

    if ( graph_delay ) {
        delay_graph_.reset( new BinnedLiveGraph( link_name + " delay [" + filename + "]",
                                                 { make_tuple( 0.0, 0.25, 0.0, 1.0, false ) },
                                                 "queueing delay (ms)",
                                                 1, false, 250,
                                                 [] ( int, int & x ) { x = -1; } ) );
    }
    base_timestamp_=timestamp();
    //base_timestamp_=0;
    //while (schedule_.at( next_delivery_ )<base_timestamp_)
         //next_delivery_ = next_delivery_ + 1;
}

void LinkQueue::record_arrival( const uint64_t arrival_time, const size_t pkt_size, bool direction __attribute__((unused)) )
{
    /* log it */
    if ( log_ ) {
        *log_ << arrival_time << " + " << pkt_size << " d: " << direction << endl;
    }

    /* meter it */
    if ( throughput_graph_ ) {
        throughput_graph_->add_value_now( 1, pkt_size );
    }
}

void LinkQueue::record_drop( const uint64_t time, const size_t pkts_dropped, const size_t bytes_dropped, bool direction __attribute__((unused)) )
{
    /* log it */
    if ( log_ ) {
        *log_ << time << " d " << pkts_dropped << " " << bytes_dropped << endl;
    }
}

void LinkQueue::record_departure_opportunity( bool direction __attribute__((unused)) )
{
    /* log the delivery opportunity */
    if ( log_ ) {
        //*log_ << next_delivery_time() << " # " << PACKET_SIZE << " index: " << next_delivery_ << " T_ts:  " << schedule_.at( next_delivery_ ) << " bts: " << base_timestamp_ << endl;
    }

    /* meter the delivery opportunity */
    if ( throughput_graph_ ) {
        throughput_graph_->add_value_now( 0, PACKET_SIZE );
    }
}

void LinkQueue::record_departure( const uint64_t departure_time, const QueuedPacket & packet, bool direction __attribute__((unused)))
{
    /* log the delivery */
    if ( log_ ) {
        *log_ << departure_time << " - " << packet.contents.size()
              << " " << departure_time - packet.arrival_time << " d: " << direction <<  " next delivery index: " << next_delivery_ << endl;
    }

    /* meter the delivery */
    if ( throughput_graph_ ) {
        throughput_graph_->add_value_now( 2, packet.contents.size() );
    }

    if ( delay_graph_ ) {
        delay_graph_->set_max_value_now( 0, departure_time - packet.arrival_time );
    }
}

void LinkQueue::read_packet( const string & contents, bool direction __attribute__((unused)) )
{
    const uint64_t now = timestamp();

    if ( contents.size() > PACKET_SIZE ) {
        throw runtime_error( "packet size is greater than maximum" );
    }

    unsigned int current_mpdu_size = cal_current_mpdu_size();

    if (packet_queue_->size_packets() > current_mpdu_size)
        rationalize( now, current_mpdu_size );

    //record_arrival( now, contents.size(), direction );
    
    if (losses_.at( next_delivery_) < 0.02)//0.02 which is residual value of losses seen in the trace
    {
        packet_queue_->enqueue( QueuedPacket( contents, now ) );
    }
}

uint64_t LinkQueue::next_delivery_time( void ) const
{
    if ( finished_ ) {
        return -1;
    } else {
        return schedule_.at( global_next_delivery_ ) + base_timestamp_; //next_delivery_
    }
}

unsigned int LinkQueue::cal_current_mpdu_size( void ) const
{
    int next_phy;
    if ( finished_ ) {
        return -1;
    } else {
        next_phy = rate_schedule_.at( next_delivery_ );
    }

    return (unsigned int) min( (int) (next_phy*3681)/(1500*8), 43 );//43
}

void LinkQueue::use_a_delivery_opportunity( bool direction __attribute__((unused)) )
{
    record_departure_opportunity(direction);

    next_delivery_ = (next_delivery_ + 1) % schedule_.size();//+1

     //*log_ << "Used an opportunity " << schedule_.size() << endl; 

    /* wraparound */
    if ( next_delivery_ == 0 ) {
        if ( repeat_ ) {
            next_delivery_=saturation_index_;
            base_timestamp_ += schedule_.back();//2500 for initial saturation phase compensation -10
        } else {
            finished_ = true;
        }
    }
    if ( global_next_delivery_ <= next_delivery_ )
        global_next_delivery_ = next_delivery_;
}

uint64_t LinkQueue::get_closest_delivery_time(uint64_t time_now)
{
    uint64_t next_delivery_time_trace = next_delivery_;
    uint64_t base_time = base_timestamp_;

    uint64_t next_delivery_time =  schedule_.at( next_delivery_time_trace ) + base_time;
    uint64_t closest_delivery_time = next_delivery_time;

    while(next_delivery_time<=time_now){
    closest_delivery_time = next_delivery_time;

    next_delivery_time_trace = (next_delivery_time_trace+1) % schedule_.size();
        /* wraparound */
        if ( next_delivery_time_trace == 0 ) {
            next_delivery_time_trace=saturation_index_;
            base_time += schedule_.back()-5;
        }
        next_delivery_time =  schedule_.at( next_delivery_time_trace ) + base_time;
    }

    return closest_delivery_time;
}

void LinkQueue::rationalize( const uint64_t now , int given_mpdu_size __attribute__((unused)) )
{
    bool send_now=true;
    //srand(now);

    if ((float)rand()/(float)(RAND_MAX) > percent_sharing_)
	{
		if (qdirection_)
            {
                if ( num_pack_up_ > 0 )
                    send_now=false;
            }
        else
            {
                if ( num_pack_down_ > 0 )
                    send_now=false;
            }
	}
    int opportunities_available = 0;

    if (send_now)
    {
        uint64_t closest_delivery_time = get_closest_delivery_time(now);
        while ( next_delivery_time() < closest_delivery_time )
            use_a_delivery_opportunity(qdirection_);
        //}

        while(next_delivery_time() <= now && opportunities_available < given_mpdu_size)
        {
                if ( packet_queue_->empty() ) {
                    break;
                    }

                use_a_delivery_opportunity(qdirection_);
                packet_in_transit_ = packet_queue_->dequeue();
                packet_in_transit_bytes_left_ += packet_in_transit_.contents.size();

                if (qdirection_==0)
                    output_queue_.emplace(  now + delay_ms_, packet_in_transit_.contents );// add delay to emulate real-world RTT only in downlink  
                else
                    output_queue_.emplace(  now, packet_in_transit_.contents );

                if (next_delivery_)
                {
                    if (sequence_numbers_.at(next_delivery_)-1 != sequence_numbers_.at(next_delivery_-1))
                        break;
                }

            assert( packet_in_transit_bytes_left_ > 0 );

            opportunities_available++;
            record_departure(now+ delay_ms_, packet_in_transit_, qdirection_);
        }

        packet_in_transit_bytes_left_=0;
    }
}

void LinkQueue::write_packets( FileDescriptor & fd, bool direction __attribute__((unused)) )
{
    while ( (not output_queue_.empty()) && (output_queue_.front().first <= timestamp()) ) {//&& (output_queue_.front().first <= timestamp()
        fd.write( output_queue_.front().second );//.second
    // *log_ << output_queue_.front().first << " tmp: " <<  timestamp() << endl;
        output_queue_.pop();
    }
    if (qdirection_)
        num_pack_up_ = output_queue_.size();
    else
        num_pack_down_ = output_queue_.size();
}

unsigned int LinkQueue::wait_time( bool direction __attribute__((unused)) )
{
    const auto now = timestamp();

    unsigned int current_mpdu_size = cal_current_mpdu_size();
    rationalize( now, current_mpdu_size);

    if ( next_delivery_time() <= now ) {
        return 0;
    } else {
        return (next_delivery_time() - now + delay_ms_);//  /2
    }
}

bool LinkQueue::pending_output( bool direction __attribute__((unused)) ) const
{
        return not output_queue_.empty();
}
