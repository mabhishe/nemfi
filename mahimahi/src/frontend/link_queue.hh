    /* -*-mode:c++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#ifndef LINK_QUEUE_HH
#define LINK_QUEUE_HH

#include <queue>
#include <cstdint>
#include <string>
#include <fstream>
#include <memory>
#include <map>

#include "file_descriptor.hh"
#include "binned_livegraph.hh"
#include "abstract_packet_queue.hh"
#include "packetshell.hh"

class LinkQueue
{
private:
    const static unsigned int PACKET_SIZE = 1504; /* default max TUN payload size */

    std::string link_name_;

    unsigned int next_delivery_;
    float percent_sharing_;
    std::vector<uint64_t> schedule_;
    std::vector<float> rate_schedule_;
    std::vector<uint64_t> sequence_numbers_;
    std::vector<float> losses_;
    uint64_t base_timestamp_;
    uint64_t delay_ms_;//rtt between the client test machine and the server

    std::unique_ptr<AbstractPacketQueue> packet_queue_;
    QueuedPacket packet_in_transit_;
    unsigned int packet_in_transit_bytes_left_;
    std::queue< std::pair<uint64_t, std::string> > output_queue_;

    std::unique_ptr<std::ofstream> log_;
    std::unique_ptr<BinnedLiveGraph> throughput_graph_;
    std::unique_ptr<BinnedLiveGraph> delay_graph_;

    bool repeat_;
    bool finished_;
    bool first_loop_;//information regarding first loop(if true at what index in trace does it hit 2.5s saturation mark)
    bool qdirection_;// true is UPLINK and false is DOWNLINK
    
    unsigned int &num_pack_up_;//to keep track of packets in either of the queues to share the link
    unsigned int &num_pack_down_;
    unsigned int &global_next_delivery_;

    PacketShell<LinkQueue> &link_shell_app_;

    unsigned int saturation_index_;

    uint64_t next_delivery_time( void ) const;

    void use_a_delivery_opportunity( bool direction );

    void record_arrival( const uint64_t arrival_time, const size_t pkt_size, bool direction);
    void record_drop( const uint64_t time, const size_t pkts_dropped, const size_t bytes_dropped, bool direction);
    void record_departure_opportunity( bool direction );
    void record_departure( const uint64_t departure_time, const QueuedPacket & packet, bool direction);

    void rationalize( const uint64_t now , int given_mpdu_size);
    void dequeue_packet( void );
    uint64_t get_closest_delivery_time(uint64_t time_now);

    unsigned int cal_current_mpdu_size( void ) const;

public:
    LinkQueue( const std::string & link_name, const std::string & filename, const std::string & logfile, const float percent_sharing,
               const bool repeat, const bool graph_throughput, const bool graph_delay,
               std::unique_ptr<AbstractPacketQueue> && packet_queue, const std::string & command_line, const bool qd, const uint64_t rtt_server,
               PacketShell<LinkQueue> &link_shell_app, unsigned int &num_pack_up, unsigned int &num_pack_down, unsigned int &global_next_delivery );

    void read_packet( const std::string & contents, bool direction);

    void write_packets( FileDescriptor & fd, bool direction);

    unsigned int wait_time( bool direction );

    bool pending_output( bool direction ) const;

    bool finished( void ) const { return finished_; }

    float percent_sharing() const { return percent_sharing_; }
};

#endif /* LINK_QUEUE_HH */
