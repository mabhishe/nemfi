#!/bin/bash
for i in {1..$1}
do
	cd ../client_saturator/multisend/sender/Client_Logs/
	filename=$(ls -ld */ | head -n ${i} | awk '{print $9}' | tail -n +${i})
	cp ${filename}/client* ../../../../mahimahi/traces/
	cd ../../../../
	cd mahimahi/src/wifi-util/
	sudo python3 convert_trace.py
	cd ../../
	#sudo tc qdisc add dev enp0s25 root tbf rate 50mbit burst 64kbit latency 800ms
	mm-link ./traces/test_file.txt ./traces/test_file.txt --uplink-log=up.log --downlink-log=down.log --percent-sharing=0.5 --rtt-server=0.85 ../protocol_application__evaluation.sh
	sudo rm ./traces/client*
done
