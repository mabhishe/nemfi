#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess, argparse, ipaddress, os
import netifaces as ni

parser = argparse.ArgumentParser(description = "IP Routes Table Configuration")
parser.add_argument('--i1', type = str, required=True, help='Name of first interface')
parser.add_argument('--i2', type = str, required=True, help='Name of second interface')
parser.add_argument('--i3', type = str, required=False, help='Name of third interface')

args = parser.parse_args()

interface_list = [args.i1,args.i2]
if args.i3 != None:
	interface_list.append(args.i3)

count = 1
for interface in interface_list:
	#Get the ip adddress
	interface_ip = ni.ifaddresses(interface)[ni.AF_INET][0]['addr']
	#Get interface netmask
	interface_netmask = ip = ni.ifaddresses(interface)[ni.AF_INET][0]['netmask']
	print (interface_netmask)
	#Get interface gateway
	interface_gateway = ""
	for el in ni.gateways()[2]:
		if el[1]==interface:
			interface_gateway=el[0]
		print ("EL ",el[1])
	#Find interface subnet
  
	a = str(interface_ip)+'/'+str(interface_netmask)
	interface_subnet = ipaddress.ip_network(str(a), strict=False)

	#Print interface details:
	print(interface+" INTERFACE DETAILS")
	print(interface+" Interface: ", interface)
	print(interface+" IP Address: ", interface_ip)
	print(interface+" Subnet Mask: ", interface_netmask)
	print(interface+" Gateway: ", interface_gateway)
	print(interface+" Subnet: ", interface_subnet)

	#Update the routing table
	file = open("/etc/iproute2/rt_tables","a+")
	file.seek(0)
	lines = file.readlines()

	rt_exists = False

	for line in lines:
		if interface in line:
			rt_exists = True

	if not rt_exists:
		file.write(str(count) +" "+interface + "\n" )

	file.close()

	#wifi_ip = str(wifi_ip)
	#wifi_subnet = str(wifi_subnet)
	prefix = str(interface_ip)+'/32'
	#iproute_interface = subprocess.Popen(['ip', 'route', 'add', str(interface_subnet), 'dev', interface, 'src', interface_ip, 'table', interface], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	iproute_interface = subprocess.Popen(['ip', 'route', 'add', str(interface_subnet), 'dev', interface, 'src', interface_ip, 'table', str(count)], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	iproute_interface.wait()
	stdout, stderr = iproute_interface.communicate()
	stderr = str(stderr)
	bool_test = True
	if (bool_test == False):
	#if iproute_interface.returncode !=0 and 'RTNETLINK answers: File exists' in stderr:
		#print(iproute_wifi.returncode, stdout, stderr)
		pass
	else:
		#iproute_interface = subprocess.Popen(['ip', 'route', 'add', 'default', 'via', interface_gateway, 'dev', interface, 'table', interface])
		iproute_interface = subprocess.Popen(['ip', 'route', 'add', 'default', 'via', interface_gateway, 'dev', interface, 'table', str(count)])
		iproute_interface.wait()
		iproute_interface = subprocess.Popen(['ip', 'rule', 'add', 'from', prefix, 'table', str(count)])
		#iproute_interface = subprocess.Popen(['ip', 'rule', 'add', 'from', prefix, 'table', interface])
		iproute_interface.wait()
		#iproute_interface = subprocess.Popen(['ip', 'rule', 'add', 'to', prefix, 'table', interface])
		iproute_interface = subprocess.Popen(['ip', 'rule', 'add', 'to', prefix, 'table', str(count)])
		iproute_interface.wait()
	count +=1
