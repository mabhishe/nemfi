#ifndef SATURATESERVO_HH
#define SATURATESERVO_HH

#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include "socket.hh"
#include "specify_params.hh"
#include "rate-estimate.hh"

class Acker;
class statem_saturatr;

class SaturateServo
{
private:
  const std::string _name;
  FILE* _log_file;

  const Socket _listen;
  const Socket _send;
  Socket::Address _remote;

  const bool _server;
  const int _send_id;

  Acker *_acker;

  RateEstimate _rate_estimator;
  static const unsigned int current_pkt_size = 1500; /* bytes */

  statem_saturatr *_statem_sat;

  uint64_t _next_transmission_time;

  static const int _transmission_interval = 1000 * 1000 * 1000;

  int _foreign_id;

  int _packets_sent, _max_ack_id;

  int _window;

  //std::map <int, int> _packet_data;
  //std::map <int, int> _loss_data;

  //long long int _last_seq_num;

  static const int LOWER_WINDOW=given_Lower_Window;
  static const int UPPER_WINDOW=given_Upper_Window;

  static constexpr double LOWER_RTT=given_Lower_RTT;
  static constexpr double UPPER_RTT=given_Upper_RTT;

public:
  SaturateServo( const char * s_name,
                 FILE* log_file,
		 const Socket & s_listen,
		 const Socket & s_send,
		 const Socket::Address & s_remote,
		 const bool s_server,
		 const int s_send_id );

  void recv( void );

  uint64_t wait_time( void ) const;

  void tick( void );

  void set_acker( Acker * const s_acker ) { _acker = s_acker; }

  void set_statem( statem_saturatr * const s_statem_sat ) { _statem_sat = s_statem_sat; }

  void set_remote( const Socket::Address & s_remote ) { _remote = s_remote; }

  SaturateServo( const SaturateServo & ) = delete;
  const SaturateServo & operator=( const SaturateServo & ) = delete;
};

//int SaturateServo::LOWER_WINDOW;
//int SaturateServo::UPPER_WINDOW;

#endif
