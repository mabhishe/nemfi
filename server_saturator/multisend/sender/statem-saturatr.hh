#ifndef STATEM_SATURATR_HH
#define STATEM_SATURATR_HH

#include <string>
#include <vector>
#include <assert.h>
#include <fstream>
#include <sys/types.h>
#include <unistd.h>

class statem_saturatr
{
private:

	/*Input variables*/

	int _mcs_uplink;
	int _mcs_downlink;
	double _bitrate_uplink;
	double _bitrate_downlink;
	std::string _wifi_standard;
	std::string _mode; //UP, DOWN or BI
	int _current_bandwidth;
	double _interface_card_sensitivity;

	/*Feedback variables*/

	double _packet_loss_wifi;
	double _packet_loss_system;
	double _monitor_duration;
	int _current_packets_flight;
	double _signal_strength;
	bool _associated;

	/*State Machine variables*/

	enum class current_state {transient, saturation, control_saturation};
	current_state _current_state;

public:
	/* Constructor and Set Methods*/

	statem_saturatr(int s_mcs_uplink,
			int s_mcs_downlink,
			double s_bitrate_uplink,
			double s_bitrate_downlink,
			std::string s_wifi_standard,
			std::string s_mode,
			int s_current_bandwidth,
			double s_interface_card_sensitivity);
	
	void set_state(int s_state);

	void update_packetloss_wifi(double s_packet_loss_wifi);
	
	void update_packetloss_system(double s_packet_loss_system);

	void set_monitor_duration(double s_monitor_duration);

	void update_current_packets_flight(int s_current_packets_flight);

	void update_signal_strength(double s_signal_strength);

	void update_associated(bool s_associated);

	/*Get Methods*/

	int get_state(void);

	double get_system_packet_loss(void);

	double get_wifi_packet_loss(void);

	int get_current_packets_flight(void);

	double get_signal_strength(void);

	bool get_associated(void);

	double get_bitrate_uplink(void) {return _bitrate_uplink;}

	double get_bitrate_downlink(void) {return _bitrate_downlink;}

	std::string get_mode(void) {return _mode;}

	bool check_saturation(double s_current_bitrate);

};


#endif
