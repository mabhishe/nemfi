#include <assert.h>

#include "rate-estimate.hh"

RateEstimate::RateEstimate( const double s_empty_rate_estimate, const unsigned int s_averaging_extent_ms )
  : empty_rate_estimate( s_empty_rate_estimate),
    averaging_extent_ms( s_averaging_extent_ms ),
    lost_percentage(0),
    history()
{
}

void RateEstimate::add_packet( const SatPayload & p )
{

  history.push( p );

  SatPayload & front = history.front();
  int seq_front=front.sequence_number;

  SatPayload & back = history.back();
  int seq_back=back.sequence_number;

  lost_percentage = std::abs((int)(seq_back-seq_front) - (int)history.size()-1);
  if((int)(seq_back - seq_front))
  	lost_percentage = lost_percentage / ((int)(seq_back-seq_front));//(int)history.size();
  //lost_percentage=1.0;
}

double RateEstimate::get_rate( void )
{
  housekeeping();
  const int num_packets = history.size();
  if ( num_packets <= 2 ) {
    return empty_rate_estimate;
  }

  return 1000.0 * (double) num_packets / (double) averaging_extent_ms;
}

void RateEstimate::housekeeping( void )
{
  const uint64_t now = Socket::timestamp();
  while ( !history.empty() ) {
    SatPayload & front = history.front();
    assert( now >= front.recv_timestamp );//recv
    if ( now - front.recv_timestamp > 1000000 * averaging_extent_ms ) {//recv
      history.pop();
    } else {
      return;
    }
  }
}
