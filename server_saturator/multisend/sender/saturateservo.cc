#include <assert.h>
#include <iostream>

#include "saturateservo.hh"
#include "socket.hh"
#include "payload.hh"
#include "acker.hh"
#include "statem-saturatr.hh"

SaturateServo::SaturateServo( const char * s_name,
                              FILE* log_file,
			      const Socket & s_listen,
			      const Socket & s_send,
			      const Socket::Address & s_remote,
			      const bool s_server,
			      const int s_send_id )
  : _name( s_name ),
    _log_file(log_file),
    _listen( s_listen ),
    _send( s_send ),
    _remote( s_remote ),
    _server( s_server ),
    _send_id( s_send_id ),
    _acker( NULL ),
    _rate_estimator( 50.0, 1000 ),
    _statem_sat(NULL),
    _next_transmission_time( Socket::timestamp() ),
    _foreign_id( -1 ),
    _packets_sent( 0 ),
    _max_ack_id( -1 ),
    _window( LOWER_WINDOW )
    /*_packet_data({{0,0}}),
    _loss_data({{0,0}}),
    _last_seq_num(0)*/
{ 
}

void SaturateServo::recv( void )
{
  /* get the ack packet */
  Socket::Packet incoming( _listen.recv() );
  SatPayload *contents = (SatPayload *) incoming.payload.data();
  contents->recv_timestamp = incoming.timestamp;

  if ( contents->sequence_number != -1 ) {
    /* not an ack */
    printf( "MARTIAN!\n" );
    return;
  }

  /* possibly roam */
  if ( _server ) {
    if ( _acker ) {
      if ( (contents->sender_id > _foreign_id) && (contents->ack_number == -1) ) {
	_foreign_id = contents->sender_id;
	_acker->set_remote( incoming.addr );
      }
    }
  }

  /* process the ack */
  if ( contents->sender_id != _send_id ) {
    /* not from us */
    return;
  } else {
    if ( contents->ack_number > _max_ack_id ) {
      _max_ack_id = contents->ack_number;
    }

    /*    printf( "%s pid=%d ACK RECEIVED senderid=%d seq=%d, send_time=%ld, recv_time=%ld\n",
	  _name.c_str(), getpid(), contents->sender_id, contents->sequence_number, contents->sent_timestamp, contents->recv_timestamp ); */

    int64_t rtt_ns = contents->recv_timestamp - contents->sent_timestamp;
    double rtt = rtt_ns / 1.e9;



    //fprintf( _log_file, "%s ACK RECEIVED senderid=%d, seq=%d, send_time=%ld,  recv_time=%ld, rtt=%.4f, %d => ",
       //_name.c_str(),_server ? _foreign_id : contents->sender_id , contents->ack_number, contents->sent_timestamp, contents->recv_timestamp, (double)rtt,  _window );



    /*Rate Estimation and updating the history of packets over last second*/

    _rate_estimator.add_packet( *contents );
    double loss_rate = (double) _rate_estimator.num_lost();//calculate loss from rate estimator
    double current_rate = _rate_estimator.get_rate() * current_pkt_size * 8.0 / 1.0e6;
    //loss_rate++;
    //current_rate++;

    fprintf( _log_file, "send_time=%ld window=%d seq=%d Mbps=%f lost=%f\n",
	    contents->sent_timestamp,
      _window,
	    contents->ack_number,
	   current_rate,
	   loss_rate*100 );

    /*uint64_t now = Socket::timestamp();

    fprintf( _log_file, "%s seq = %d \n",
      _name.c_str(),
      contents->ack_number);*/


    /* increase-decrease rules */

        if ( (rtt < LOWER_RTT) && (_window < UPPER_WINDOW) && !(contents->ack_number%1000)) 
        {
          if (_window>0)
            _window+= (int)(300*0.35 - current_rate)*0.1 -0.5*loss_rate ;//100
        }

    if ( (rtt > UPPER_RTT) && (_window > LOWER_WINDOW + 10) ) {
      _window -= 20;
    }

    //fprintf( _log_file, "%d\n", _window );
  }
}

uint64_t SaturateServo::wait_time( void ) const
{
  int num_outstanding = _packets_sent - _max_ack_id - 1;

  if ( _remote == UNKNOWN ) {
    return 1000000000;
  }

  if ( num_outstanding < _window ) {
    return 0;
  } else {
    int diff = _next_transmission_time - Socket::timestamp();
    if ( diff < 0 ) {
      diff = 0;
    }
    return diff;
  }
}

void SaturateServo::tick( void )
{
	//fprintf(_log_file, "===========================================================================%lf\n", _statem_sat->get_bitrate_uplink());
  if ( _remote == UNKNOWN ) {
    return;
  }

  int num_outstanding = _packets_sent - _max_ack_id - 1;

  if ( num_outstanding < _window ) {
    /* send more packets */
    int amount_to_send = _window - num_outstanding;
    for ( int i = 0; i < amount_to_send; i++ ) {
      SatPayload outgoing;
      outgoing.sequence_number = _packets_sent;
      outgoing.ack_number = -1;
      outgoing.sent_timestamp = Socket::timestamp();
      outgoing.recv_timestamp = 0;
      outgoing.sender_id = _send_id;

      _send.send( Socket::Packet( _remote, outgoing.str( 1400 ) ) );

      /*
      printf( "%s pid=%d DATA SENT %d senderid=%d seq=%d, send_time=%ld, recv_time=%ld\n",
      _name.c_str(), getpid(), amount_to_send, outgoing.sender_id, outgoing.sequence_number, outgoing.sent_timestamp, outgoing.recv_timestamp ); */

      _packets_sent++;
    }

    _next_transmission_time = Socket::timestamp() + _transmission_interval;
  }

  if ( _next_transmission_time < Socket::timestamp() ) {
    SatPayload outgoing;
    outgoing.sequence_number = _packets_sent;
    outgoing.ack_number = -1;
    outgoing.sent_timestamp = Socket::timestamp();
    outgoing.recv_timestamp = 0;
    outgoing.sender_id = _send_id;

    _send.send( Socket::Packet( _remote, outgoing.str( 1400 ) ) );

    /*
    printf( "%s pid=%d DATA SENT senderid=%d seq=%d, send_time=%ld, recv_time=%ld\n",
    _name.c_str(), getpid(), outgoing.sender_id, outgoing.sequence_number, outgoing.sent_timestamp, outgoing.recv_timestamp ); */

    _packets_sent++;

    _next_transmission_time = Socket::timestamp() + _transmission_interval;
  }
}
