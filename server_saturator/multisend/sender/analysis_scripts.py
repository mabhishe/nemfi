# -*- coding: utf-8 -*-

import re
import csv
import matplotlib.pyplot as plt
import numpy as np
from os import listdir
from os.path import isfile, join

class sat_analysis():
	def get_ThroughputandLoss(self, path):
		files = [f for f in listdir(path) if isfile(join(path, f))]
		for i in range(len(files)):
			if files[i].startswith('client'):
				with open(join(path, files[i])) as f:
					counter1 = 0
					counter2 =0
					for line in f:
						counter1 += line.count("INCOMING")
						counter2 += line.count("OUTGOING")
					f.close()
				print("Number of packets received on client side: ", counter1)
				print("Number of packets acks received from client side: ", counter2)

			elif (files[i].startswith('server')):
				with open(join(path, files[i])) as f:
					counter1 = 0
					counter2 =0
					for line in f:
						counter1 += line.count("INCOMING")
						counter2 += line.count("OUTGOING")
					f.close()
				print("Number of packets received on server side: ", counter1)
				print("Number of acks received from server side: ", counter2)

			packets_sent = list()
			packets_recv = list()
			time_diff = list()
			throughput = list()
			seqno = list()
			packetloss = list()

			index=i

			if files[i].startswith('client'):
				for line in open(join(path, files[i])).readlines():
					if "send_time" in line:
						send_time = line
						if "INCOMING" in send_time:
							split_line = send_time.split()
							send_time = split_line[5]
							send_time = re.findall(r'[A-Za-z]|-?\d+\.\d+|\d+',send_time)
							send_time = float(send_time[8])
							packets_sent.append(send_time)

							recv_time = split_line[6]
							recv_time = re.findall(r'[A-Za-z]|-?\d+\.\d+|\d+',recv_time)
							recv_time = float(recv_time[8])
							packets_recv.append(recv_time)

							seq = split_line[4]
							seq = re.findall(r'[A-Za-z]|-?\d+\.\d+|\d+',seq)
							seq = int(seq[3])
							seqno.append(seq)

				for i in range(len(packets_sent)-1):
					time_diff.append((packets_recv[i+1]-packets_recv[i])/1000000)

				i = 0
				l = 0
				"""print (max(packets_recv) - min(packets_sent))/1000000"""
				seqno.sort()
				while i < len(time_diff)-1:
					if(time_diff[i]>=1000):
						loss = 0
						a = (2)*1500*8/(time_diff[i]*1000)
						l = seqno[i+1] - seqno[i]
						if l != 1:
							loss = l - 1
						i += 1
						throughput.append(a)
						loss = ((loss*1.0)/(loss+2)) * 100
						packetloss.append(loss)
					else:
						j = time_diff[i]
						k = 1
						l = 0
						loss = 0
						while j<=1000 and i<len(time_diff)-1:
							j = j + time_diff[i+1]
							l = seqno[i+1] - seqno[i]
							if l != 1:
								loss = loss + l - 1
							i += 1
							k += 1
						#print(j)

						a = ((k+1)*1500*8)/(j*1000)
						throughput.append(a)
						loss = ((loss*1.0)/((loss+k)) * 100)
						packetloss.append(loss)

				throughput.insert(0,0)
				packetloss.insert(0,0)

				times = list()
				t = 0
				for i in range(len(throughput)):
					times.append(t)
					t = t + 1
					t = round(t,1)

				plt.figure(1)
				plt.plot(times, throughput, label='server')
				plt.savefig('throughput_server')

				plt.figure(2)
				plt.plot(times, packetloss, label='server')
				plt.savefig('packetloss_server')

			elif files[index].startswith('server'):
				for line in open(join(path, files[i])).readlines():
					if "send_time" in line:
						send_time = line
						if "INCOMING" in send_time:
							split_line = send_time.split()
							send_time = split_line[5]
							send_time = re.findall(r'[A-Za-z]|-?\d+\.\d+|\d+',send_time)
							send_time = float(send_time[8])
							packets_sent.append(send_time)

							recv_time = split_line[6]
							recv_time = re.findall(r'[A-Za-z]|-?\d+\.\d+|\d+',recv_time)
							recv_time = float(recv_time[8])
							packets_recv.append(recv_time)

							seq = split_line[4]
							seq = re.findall(r'[A-Za-z]|-?\d+\.\d+|\d+',seq)
							seq = int(seq[3])
							seqno.append(seq)

				for i in range(len(packets_sent)-1):
					time_diff.append((packets_recv[i+1]-packets_recv[i])/1000000)

				i = 0
				l = 0
				seqno.sort()
				while i < len(time_diff)-1:
					if(time_diff[i]>=1000):
						loss = 0
						a = (2)*1500*8/(time_diff[i]*1000)
						l = seqno[i+1] - seqno[i]
						if l != 1:
							loss = l - 1
						i += 1
						throughput.append(a)
						loss = ((loss*1.0)/((loss+2)) * 100)
						packetloss.append(loss)
					else:
						j = time_diff[i]
						k = 1
						l = 0
						loss = 0
						while j<=1000 and i<len(time_diff)-1:
							j = j + time_diff[i+1]
							l = seqno[i+1] - seqno[i]
							if l != 1:
								loss = loss + l - 1
							i += 1
							k += 1
						#print(j)

						a = ((k+1)*1500*8)/(j*1000)
						throughput.append(a)
						loss = ((loss*1.0)/((loss+k)) * 100)
						packetloss.append(loss)

				throughput.insert(0,0)
				packetloss.insert(0,0)

				times = list()
				t = 0
				for i in range(len(throughput)):
					times.append(t)
					t = t + 1
					t = round(t,1)

				plt.figure(3)
				plt.plot(times, throughput, label='client')
				plt.savefig('throughput_client')

				plt.figure(4)
				plt.plot(times, packetloss, label='client')
				plt.savefig('packetloss_client')

def main():

	sat_trace = sat_analysis()
	sat_trace.get_ThroughputandLoss('/Users/mabhishe/traces')

if __name__ == '__main__':
	main()