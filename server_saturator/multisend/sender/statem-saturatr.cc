#include "statem-saturatr.hh"
#include <string.h>

statem_saturatr::statem_saturatr(int s_mcs_uplink,
			int s_mcs_downlink,
			double s_bitrate_uplink,
			double s_bitrate_downlink,
			std::string s_wifi_standard,
			std::string s_mode,
			int s_current_bandwidth,
			double s_interface_card_sensitivity)
: _mcs_uplink(s_mcs_uplink),
  _mcs_downlink(s_mcs_downlink),
  _bitrate_uplink(s_bitrate_uplink),
  _bitrate_downlink(s_bitrate_downlink),
  _wifi_standard(s_wifi_standard),
  _mode(s_mode),
  _current_bandwidth(s_current_bandwidth),
  _interface_card_sensitivity(s_interface_card_sensitivity),
  _packet_loss_wifi(0),
  _packet_loss_system(0),
  _monitor_duration(1.0),
  _current_packets_flight(0),
  _signal_strength(17),
  _associated(false),
  _current_state(current_state::transient)
 {
 }

void statem_saturatr::set_state(int s_state)
{
	_current_state = static_cast<current_state>(s_state);
}

void statem_saturatr::update_packetloss_wifi(double s_packet_loss_wifi)
{
	_packet_loss_wifi = s_packet_loss_wifi;
}

void statem_saturatr::update_packetloss_system(double s_packet_loss_system)
{
	_packet_loss_system = s_packet_loss_system;
}

void statem_saturatr::set_monitor_duration(double s_monitor_duration)
{
	_monitor_duration = s_monitor_duration;
}

void statem_saturatr::update_current_packets_flight(int s_current_packets_flight)
{
	_current_packets_flight=s_current_packets_flight;
}

void statem_saturatr::update_signal_strength(double s_signal_strength)
{
	_signal_strength=s_signal_strength;
}

void statem_saturatr::update_associated(bool s_associated)
{
	_associated = s_associated;
}

int statem_saturatr::get_state(void)
{
	return static_cast<int>(_current_state);
}

double statem_saturatr::get_system_packet_loss(void)
{
	return _packet_loss_system;
}

double statem_saturatr::get_wifi_packet_loss(void)
{
	return _packet_loss_wifi;
}

int statem_saturatr::get_current_packets_flight(void)
{
	return _current_packets_flight;
}

double statem_saturatr::get_signal_strength(void)
{
	return _signal_strength;
}

bool statem_saturatr::get_associated(void)
{
	return _associated;
}

bool statem_saturatr::check_saturation(double s_current_bitrate)
{
	/*if (!strcmp(_mode, "UP"))
	{
		if (s_current_bitrate>=(0.8*_bitrate_uplink)) return true;
	}
	else if (!strcmp(_mode, "BI"))
	{
		if (s_current_bitrate>=(0.3*_bitrate_uplink)) return true;
	}
	else if (!strcmp(_mode, "DOWN"))
	{
		return false;
	}*/
	s_current_bitrate++;
	return false;
}