#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess, time, shutil
import argparse, os
from os import path

import re
#import matplotlib.pyplot as plt
#import numpy as np
from os import listdir
from os.path import isfile, join

def set_sat_params(UPPER_WINDOW, LOWER_WINDOW=5, LOWER_RTT=1.5, UPPER_RTT=3.0):
	print('Setting Saturator parameter, Upper Window: ', UPPER_WINDOW)
	file = open("specify_params.hh", "w")
	file.write("const int given_Lower_Window={};".format(LOWER_WINDOW) + '\n')
	file.write("const int given_Upper_Window={};".format(UPPER_WINDOW) + '\n')
	file.write("constexpr double given_Lower_RTT={};".format(LOWER_RTT) + '\n')
	file.write("constexpr double given_Upper_RTT={};".format(UPPER_RTT) + '\n')
	file.close()

def saturator(duration, server_mode, wind_val):
	get_current_directory = subprocess.Popen(['pwd'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
	cwd_out, cwd_err = get_current_directory.communicate()
	cwd_out = cwd_out.decode('utf-8')[:-1]

	#clean_make = subprocess.Popen(['make', '-C', '/home/mimove/sprout/multisend/sender/', 'clean'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
	#clean_make.wait()
	update_make = subprocess.Popen(['make', 'saturatr'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True, cwd=cwd_out)
	#print("Make Updated")
	stdout, stderr = update_make.communicate()
	#print(stdout, stderr)

	start_time = time.time()
	sat_wifi = subprocess.Popen(['nice', '-19', cwd_out + '/saturatr', server_mode], stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
	print('saturator wifi server')		
	while (time.time() - start_time < duration):
		pass
	sat_wifi.terminate()
	stdout_wifi, stderr_wifi = sat_wifi.communicate()

	folder = "Server_Logs/Data_"+str(int(time.time()))+"_"+server_mode+"_"+ str(wind_val) + "/"
	if not os.path.exists(folder):
		os.makedirs(folder)

	source = os.getcwd()
	files = [i for i in os.listdir(source) if i.startswith("server-") and path.isfile(path.join(source, i))]
	for f in files:
		shutil.move(path.join(source, f), folder)

	directory="Server_Logs"
	os.chmod(directory, 0o777)
	os.chmod(folder, 0o777)

	return stdout_wifi

def downlink_iperf(destination, client_port):

	global folder
	timeStr = int(time.time())
	sender_id = int(os.getpid())

	filename_wifi = "{0}_{1}_{2}".format("server_iperf_wifi",timeStr,sender_id)
	file_wifi = open(filename_wifi, "w")

	iperf_wifi = subprocess.Popen(['iperf', '-c', destination, '-i', '10',  '-p', client_port, '-t', 30], stdout=file_wifi)
	print('iperf wifi')

	iperf_wifi.wait()
	iperf_wifi.send_signal(signal.SIGINT)
	file_wifi.close()

	source = os.getcwd()
	files = [i for i in os.listdir(source) if i.startswith("server-") and path.isfile(path.join(source, i))]
	for f in files:
		shutil.move(path.join(source, f), folder)


def main():

	parser = argparse.ArgumentParser(description = "Data Collection Server Side - for Saturator")
	parser.add_argument('T', type=int, help='Time Duration in Seconds')
	parser.add_argument('dest', type=str, help='Client IP Address')
	parser.add_argument('mode', type=str, help='Mode of Transmissions: UP, DOWN or BI')
	parser.add_argument('test_lwin', type = int, help='Test Lower Window')
	parser.add_argument('test_uwin', type = int, help='Test Upper Window')
	parser.add_argument('--iprf', type = bool, help='Run Iperf Experiment - BOOLEAN', required = False, default = False)
	parser.add_argument('--anlyz', type = bool, help='Perform Analysis over data - BOOLEAN', required = False, default = False)
	parser.add_argument('--iperf_port', type = int, help='Iperf Sever Port', required = False, default = 20000)

	args = parser.parse_args()

	duration = int(args.T) # time duration in seconds
	client_port = args.iperf_port
	destination = args.dest
	server_mode = args.mode
	do_analysis = args.anlyz

	directory = "Server_Logs"
	if not os.path.exists(directory):
		os.makedirs(directory)

	source = os.getcwd()
	files = [i for i in os.listdir(source) if i.startswith("server-")]
	for f in files:
		if os.path.isfile(f):
			os.remove(f)

	window_list=[]
	step=1000
	for val in range(args.test_lwin, args.test_uwin+1, step):
		window_list.append(val)

	for val in window_list:
		set_sat_params(val)
		client_id = saturator(duration, server_mode, val)
		time.sleep(1)
	#client_id = str(client_id)
	#client_id = client_id[2:len(client_id)-1]

	if args.iprf:
		uplink_iperf(destination, client_port)
		print("Iperf Server Test Completed")

	print("TESTING DONE: TRACES COLLECTED")

	print("COMPLETED")

if __name__ == '__main__':
	main()
