#!/bin/bash

sudo sysctl -w net.core.rmem_max=33554432 #16777216
sudo sysctl -w net.core.wmem_max=33554432 #16777216
sudo sysctl -w net.core.rmem_default=8388608
sudo sysctl -w net.core.wmem_default=8388608
